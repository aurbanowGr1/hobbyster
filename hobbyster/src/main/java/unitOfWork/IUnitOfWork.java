package unitOfWork;

import domain.EntityBase;

public interface IUnitOfWork {

	public void commit();
	public void rollback();
	public void markAsNew(EntityBase entity, IUnitOfWorkRepository repo);
	public void markAsChanged(EntityBase entity, IUnitOfWorkRepository repo);
	public void markAsDeleted(EntityBase entity, IUnitOfWorkRepository repo); 
	
}
