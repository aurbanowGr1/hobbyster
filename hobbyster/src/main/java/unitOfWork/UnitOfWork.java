package unitOfWork;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.*;

import domain.EntityBase;
import domain.EntityState;

public class UnitOfWork implements IUnitOfWork {

	private Map<EntityBase, IUnitOfWorkRepository> entities = new HashMap<EntityBase,IUnitOfWorkRepository>();
	Connection connection;
	
	public UnitOfWork(Connection connection)
	{
		this.connection=connection;
		try {
			connection.setAutoCommit(false);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	
	public void commit() {
		for(EntityBase entity: entities.keySet())
		{
			switch(entity.getState())
			{
			case New: 
				entities.get(entity).persistAdd(entity);
			case Changed:
				entities.get(entity).persistUpdate(entity);
				break;
			case Deleted:
				entities.get(entity).persistDelete(entity);
				break;
			case Unchanged:
				break;
			default:
				break;
			}
		}
		try
		{
			connection.commit();
		} catch (SQLException e) {
				e.printStackTrace();
			}
		entities.clear();
	}

	public void rollback() {
		try {
			connection.rollback();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		entities.clear();
		
	}

	public void markAsNew(EntityBase entity,IUnitOfWorkRepository repo) {
		entity.setState(EntityState.New);
		entities.put(entity, repo);
	}

	public void markAsChanged(EntityBase entity,IUnitOfWorkRepository repo) {
		entity.setState(EntityState.Changed);
		entities.put(entity, repo);
		
	}

	public void markAsDeleted(EntityBase entity, IUnitOfWorkRepository repo) {
		entity.setState(EntityState.Deleted);
		entities.put(entity, repo);
		
	}


	

}
