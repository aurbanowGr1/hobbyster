package repositoryDb;

import java.util.*;

import domain.EntityBase;

public interface IRepository<TClass extends EntityBase> {

	public TClass get(int id);
	public List<TClass> getAll();
	
	public void add(TClass field);
	public void delete(TClass field);
	public void update(TClass field);
	
}
