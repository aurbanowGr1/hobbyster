package repositoryDb;

import java.util.*;

import domain.*;

public interface IForumRepository extends IRepository<Forum> {

	public List<Forum> getForumWithForumName(String forumName);
}
