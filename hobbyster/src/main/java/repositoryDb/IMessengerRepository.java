package repositoryDb;

import java.util.List;
import domain.Messenger;

public interface IMessengerRepository extends IRepository<Messenger> {

	public List<Messenger> getMessageFromUsersNick(String nick);
	public List<Messenger> getMessageFromForum(String forumName);
	
}
