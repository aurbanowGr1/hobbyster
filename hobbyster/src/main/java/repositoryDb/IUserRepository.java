package repositoryDb;

import java.util.*;

import domain.*;

public interface IUserRepository extends IRepository<User>
{
	
	public User getUsersWithNick(String nick);
	public List<User> getUsersWithName(String name);
	public List<User> getUsersAtAge(String age);
	public List<User> getUsersFromCity(String city);
}
