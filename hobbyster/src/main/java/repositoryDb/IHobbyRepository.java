package repositoryDb;

import java.util.*;

import domain.*;


public interface IHobbyRepository extends IRepository<Hobby>
{
	public List<Hobby> getHobbysWithHobbysName(String interestedIn);
}
