package repositoryDb.impl;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import domain.Forum;
import domain.Hobby;
import domain.Messenger;
import domain.User;
import repositoryDb.IRepository;
import repositoryDb.IRepositoryCatalog;

@Stateless
public class RepositoryCatalog implements IRepositoryCatalog {
	
	@Inject
	private EntityManager em;

	@Override
	public IRepository<User> getUser() {
		return new UserDbRepository(em, User.class);
	}

	@Override
	public IRepository<Forum> getForum() {
		return new ForumDbRepository(em, Forum.class);
	}

	@Override
	public IRepository<Hobby> getHobby() {
		return new HobbyDbRepository(em, Hobby.class);
	}

	@Override
	public IRepository<Messenger> getMessenger() {
		return new MessengerDbRepository(em, Messenger.class);
	}



	}
