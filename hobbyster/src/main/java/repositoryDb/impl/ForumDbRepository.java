package repositoryDb.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import repositoryDb.IForumRepository;
import domain.Forum;

public class ForumDbRepository extends DbRepository<Forum> implements IForumRepository{

	public ForumDbRepository(EntityManager em, Class<Forum> entity)
	{
		super(em, entity);
	}
	
	protected String getTableName()
	{
		return "Forum";
	}

	@Override
	public List<Forum> getForumWithForumName(String forumName) {
		  TypedQuery<Forum> query =
			      em.createNamedQuery("Forum.findByForumName", Forum.class);
		  query.setParameter(1, forumName);
		    try {
		      return query.getResultList();
		    } 
		    catch (NoResultException e) {
		      return null;
		      }
	}

	@Override
	public List<Forum> getAll() {
		return em.createNamedQuery("Forum.getAll", Forum.class).getResultList();
	}

	
}
