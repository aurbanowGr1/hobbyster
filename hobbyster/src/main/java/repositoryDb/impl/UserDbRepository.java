package repositoryDb.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import repositoryDb.IUserRepository;
import domain.User;

public class UserDbRepository extends DbRepository<User> implements IUserRepository {
	
	public UserDbRepository(EntityManager em, Class<User> entity)
	{
		super(em, entity);
	}
	
	protected String getTableName()
	{
		return "User";
	}

	@Override
	public User getUsersWithNick(String nick) {
		Query query =
			      em.createNamedQuery("User.findByNick", User.class);
		 query.setParameter(1, nick);
		    try {
		      return (User) query.getSingleResult();
		    } catch (NoResultException e) {
		      return null;}
	}

	@Override
	public List<User> getUsersWithName(String name) {
		TypedQuery<User> query =
			      em.createNamedQuery("User.findByName", User.class);
		 query.setParameter(1, name);
		    try {
		      return query.getResultList();
		    } 
		    catch (NoResultException e) {
		      return null;
		      }
	}

	@Override
	public List<User> getUsersAtAge(String age) {
		TypedQuery<User> query =
			      em.createNamedQuery("User.findByAge", User.class);
		 query.setParameter(1, age);
		    try {
		      return query.getResultList();
		    } 
		    catch (NoResultException e) {
		      return null;
		      }
	}

	@Override
	public List<User> getUsersFromCity(String city) {
		TypedQuery<User> query =
			      em.createNamedQuery("User.findByCity", User.class);
			query.setParameter(1, city);
			    try {
			      return query.getResultList();
			    } 
			    catch (NoResultException e) {
			      return null;
			      }
	}

	@Override
	public List<User> getAll() {
			return em.createNamedQuery("User.getAll", User.class).getResultList();
		}


}