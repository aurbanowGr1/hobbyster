package repositoryDb.impl;

import java.util.List;

import javax.persistence.EntityManager;

import repositoryDb.IMessengerRepository;
import domain.Messenger;

public class MessengerDbRepository  extends DbRepository<Messenger> implements IMessengerRepository  {

	public MessengerDbRepository(EntityManager em, Class<Messenger> entity)
	{
		super(em, entity);
	}
	
	protected String getTableName()
	{
		return "Messenger";
	}

	

	@Override
	public List<Messenger> getMessageFromUsersNick(String nick) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Messenger> getMessageFromForum(String forumName) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Messenger> getAll() {
		return em.createNamedQuery("Messenger.getAll", Messenger.class).getResultList();
	}
	
	
	
}
