package repositoryDb.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import repositoryDb.IHobbyRepository;
import domain.Hobby;

public class HobbyDbRepository extends DbRepository<Hobby> implements IHobbyRepository {
	
	public HobbyDbRepository(EntityManager em, Class<Hobby> entity)
	{
		super(em, entity);
	}
	
	protected String getTableName()
	{
		return "Hobby";
	}

	@Override
	public List<Hobby> getHobbysWithHobbysName(String interestedIn) {
		TypedQuery<Hobby> query =
			      em.createNamedQuery("Hobby.findByHobby", Hobby.class);
		 query.setParameter(1, interestedIn);
		    try {
		      return query.getResultList();
		    } 
		    catch (NoResultException e) {
		      return null;
		      }
	}

	@Override
	public List<Hobby> getAll() {
		return em.createNamedQuery("Hobby.getAll", Hobby.class).getResultList();
	}


}
