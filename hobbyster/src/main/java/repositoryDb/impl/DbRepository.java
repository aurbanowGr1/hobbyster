package repositoryDb.impl;

import javax.persistence.EntityManager;
import repositoryDb.IRepository;
import domain.EntityBase;

public abstract class DbRepository<TClass extends EntityBase> implements IRepository<TClass>
{

	protected EntityManager em;
	final Class<TClass> entity;
	
	public DbRepository(EntityManager em, Class<TClass> entity) {
				this.em=em;
				this.entity=entity;

	}

	@Override
	public TClass get(int id)
	{
		return em.find(entity, id);
	
	}
	
		
	@Override
	public void add(TClass entity)
	{
		em.persist(entity);
	}
	
	@Override
	public void delete(TClass entity)
	{
	    em.remove(entity);
	}
	
	@Override
	public void update(TClass entity) {

	}

	protected abstract String getTableName();

}
