package repositoryDb;

import domain.Forum;
import domain.Hobby;
import domain.Messenger;
import domain.User;


public interface IRepositoryCatalog {
	
	public IRepository<User> getUser();
	public IRepository<Forum> getForum();
	public IRepository<Hobby> getHobby();
	public IRepository<Messenger> getMessenger();

	
}
