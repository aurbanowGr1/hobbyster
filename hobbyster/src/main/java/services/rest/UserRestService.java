package services.rest;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;





import domain.User;
import repositoryDb.IRepositoryCatalog;
import services.dto.UserDto;
import services.dto.UserSummaryDto;

@Path("users")
public class UserRestService {

	@Inject
	IRepositoryCatalog catalog;
	
	@GET
	@Path("/get")
	@Produces("application/json")
	@Transactional
	public UserSummaryDto get()
	{
		UserSummaryDto result = new UserSummaryDto();
		User user = catalog.getUser().get(1);
		result.setId(user.getId());
		result.setNick(user.getNick());
		return result;
	}
	
	@GET
	@Path("/getAll")
	@Produces("application/json")
	@Transactional
	public List<UserSummaryDto> getUsers()
	{
		List<UserSummaryDto> result = new ArrayList<UserSummaryDto>();
		List<User> users = catalog.getUser().getAll();
		for(User u : users)
		{
			UserDto user = new UserDto();
			user.setAge(u.getAge());
			user.setCity(u.getCity());
			user.setMail(u.getMail());
			user.setNick(u.getNick());
			user.setPassword(u.getPassword());
			user.setId(u.getId());
			result.add(user);
		}
		return result;
	}
	
	
	@GET
	@Path("/test")
	@Produces("text/html")
	public String test()
	{
		return "rest service is running";
	}
	
	
	
	@POST
	@Path("/add")
	@Consumes("application/json")
	@Transactional
	public String saveUser(UserSummaryDto user)
	{
		User u = new User();
		u.setNick(user.getNick());
		u.setAge(user.getAge());
		u.setCity(user.getCity());
		u.setMail(user.getMail());
		u.setName(user.getName());
		u.setPassword(user.getPassword());
		
		catalog.getUser().add(u);
		
		return "ok";
		
	}
	
}
