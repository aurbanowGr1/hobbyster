package services.rest;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import repositoryDb.IRepositoryCatalog;
import services.dto.HobbySummaryDto;
import domain.Hobby;
@Path("hobby")
public class HobbyRestService {

		@Inject
		IRepositoryCatalog catalog;
		
		@GET
		@Path("/get")
		@Produces("application/json")
		@Transactional
		public HobbySummaryDto get()
		{
			HobbySummaryDto result = new HobbySummaryDto();
			Hobby hobby = catalog.getHobby().get(1);
			result.setId(hobby.getId());
			result.setInterestedIn(hobby.getInterestedIn());
			return result;
		}
		
		@GET
		@Path("/getAll")
		@Produces("application/json")
		@Transactional
		public List<HobbySummaryDto> getHobby()
		{
			List<HobbySummaryDto> result = new ArrayList<HobbySummaryDto>();
			List<Hobby> hobby = catalog.getHobby().getAll();
			for(Hobby h : hobby)
			{
				HobbySummaryDto hobbyy = new HobbySummaryDto();
				hobbyy.setInterestedIn(h.getInterestedIn());
				hobbyy.setId(h.getId());
				result.add(hobbyy);
			}
			return result;
		}
		
		@GET
		@Path("/test")
		@Produces("text/html")
		public String test()
		{
			return "rest service is running";
		}
		
		
		
		@POST
		@Path("/add")
		@Consumes("application/json")
		@Transactional
		public String saveHobby(HobbySummaryDto hobby)
		{
			Hobby h = new Hobby();
			h.setInterestedIn(hobby.getInterestedIn());
			catalog.getHobby().add(h);
			
			return "ok";
			
		}
}
