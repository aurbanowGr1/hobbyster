package services.rest;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import repositoryDb.IRepositoryCatalog;
import services.dto.ForumSummaryDto;
import domain.Forum;

@Path("forum")
public class ForumRestService {

	@Inject
	IRepositoryCatalog catalog;
	
	@GET
	@Path("/get")
	@Produces("application/json")
	@Transactional
	public ForumSummaryDto get()
	{
		ForumSummaryDto result = new ForumSummaryDto();
		Forum forum = catalog.getForum().get(1);
		result.setId(forum.getId());
		result.setForumName(forum.getForumName());
		return result;
	}
	
	@GET
	@Path("/test")
	@Produces("text/html")
	public String test()
	{
		return "rest service is running";
	}
	
	
	
	@POST
	@Path("/add")
	@Consumes("application/json")
	@Transactional
	public String saveForum(ForumSummaryDto forum)
	{
		Forum f = new Forum();
		f.setForumName(forum.getForumName());
		catalog.getForum().add(f);
		
		return "ok";
		
	}
}
