package services.rest;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import repositoryDb.IRepositoryCatalog;
import services.dto.MessengerSummaryDto;
import domain.Messenger;

@Path("message")
public class MessengerRestService {

	
	@Inject
	IRepositoryCatalog catalog;
	
	@GET
	@Path("/get")
	@Produces("application/json")
	@Transactional
	public MessengerSummaryDto get()
	{
		MessengerSummaryDto result = new MessengerSummaryDto();
		Messenger message = catalog.getMessenger().get(1);
		result.setId(message.getId());
		result.setMessage(message.getMessage());
		return result;
	}
	
	@GET
	@Path("/test")
	@Produces("text/html")
	public String test()
	{
		return "rest service is running";
	}
	@GET
	@Path("/getAll")
	@Produces("application/json")
	@Transactional
	public List<MessengerSummaryDto> getMessages()
	{
		List<MessengerSummaryDto> result = new ArrayList<MessengerSummaryDto>();
		List<Messenger> messages = catalog.getMessenger().getAll();
		for(Messenger m : messages)
		{
			MessengerSummaryDto message = new MessengerSummaryDto();
			message.setMessage(m.getMessage());
			message.setId(m.getId());;
			result.add(message);
		}
		return result;
	}
	
	
	
	@POST
	@Path("/add")
	@Consumes("application/json")
	@Transactional
	public String saveMessage(MessengerSummaryDto message)
	{
		Messenger m= new Messenger();
		m.setMessage(message.getMessage());
		catalog.getMessenger().add(m);
		
		return "ok";
		
	}
}
