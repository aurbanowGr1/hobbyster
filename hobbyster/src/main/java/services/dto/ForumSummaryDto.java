package services.dto;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ForumSummaryDto {
	
	private int id;
	private String forumName;

	public String getForumName() {
		return forumName;
	}

	public void setForumName(String forumName) {
		this.forumName = forumName;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	
}
