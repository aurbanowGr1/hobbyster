package services.dto;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class HobbySummaryDto {

	private int id;
	private String interestedIn;

	public String getInterestedIn() {
		return interestedIn;
	}

	public void setInterestedIn(String interestedIn) {
		this.interestedIn = interestedIn;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	
	
}
