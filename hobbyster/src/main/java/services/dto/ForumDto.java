package services.dto;

import java.util.ArrayList;

public class ForumDto {

private ArrayList<MessengerSummaryDto> msd;
private ArrayList<UserSummaryDto> usd;
	
	public ForumDto()
	{
		this.msd = new ArrayList<MessengerSummaryDto>();
		this.usd = new ArrayList<UserSummaryDto>();
	}
	
	public ArrayList<MessengerSummaryDto> getMsd()
	{
		return msd;
	}
	
	public void setMsd(ArrayList<MessengerSummaryDto> msd)
	{
		this.msd=msd;
	}

	public ArrayList<UserSummaryDto> getUsd() {
		return usd;
	}

	public void setUsd(ArrayList<UserSummaryDto> usd) {
		this.usd = usd;
	}
	
	
}
