package services.dto;

import java.util.ArrayList;

public class UserDto extends UserSummaryDto{

	private ArrayList<MessengerSummaryDto> msd;
	private ArrayList<HobbySummaryDto> hsd;
	private ArrayList<ForumSummaryDto> gsd;
	
	public UserDto()
	{
		this.msd = new ArrayList<MessengerSummaryDto>();
		this.hsd = new ArrayList<HobbySummaryDto>();
		this.gsd = new ArrayList<ForumSummaryDto>();
		
	}
	
	public ArrayList<MessengerSummaryDto> getMsd()
	{
		return msd;
	}
	
	public void setMsd(ArrayList<MessengerSummaryDto> msd)
	{
		this.msd=msd;
	}

	public ArrayList<HobbySummaryDto> getHsd() {
		return hsd;
	}

	public void setHsd(ArrayList<HobbySummaryDto> hsd) {
		this.hsd = hsd;
	}

	public ArrayList<ForumSummaryDto> getGsd() {
		return gsd;
	}

	public void setGsd(ArrayList<ForumSummaryDto> gsd) {
		this.gsd = gsd;
	}
	
	
}
