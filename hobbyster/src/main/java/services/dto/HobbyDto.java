package services.dto;

import java.util.ArrayList;

public class HobbyDto {

	
private ArrayList<ForumSummaryDto> gsd;
private ArrayList<UserSummaryDto> usd;
	
	public HobbyDto()
	{
		this.gsd = new ArrayList<ForumSummaryDto>();
		this.usd = new ArrayList<UserSummaryDto>();
	}
	
	public ArrayList<ForumSummaryDto> getGsd()
	{
		return gsd;
	}
	
	public void setGsd(ArrayList<ForumSummaryDto> gsd)
	{
		this.gsd=gsd;
	}

	public ArrayList<UserSummaryDto> getUsd() {
		return usd;
	}

	public void setUsd(ArrayList<UserSummaryDto> usd) {
		this.usd = usd;
	}
	
	
}
