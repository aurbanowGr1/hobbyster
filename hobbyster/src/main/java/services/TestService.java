package services;

import javax.inject.Inject;
import javax.jws.WebService;
import javax.transaction.Transactional;

import repositoryDb.IRepositoryCatalog;
import domain.Hobby;
import domain.User;

@WebService()
public class TestService {

	@Inject
	private IRepositoryCatalog irp;
	
	@Transactional
	public int test()
	{
		try{
			User user = new User();
			user.setNick("Bart");
			user.setPassword("xyz");
			user.setAge("22");
			irp.getUser().add(user);
			
		}catch(Exception ex)
		{
			ex.printStackTrace();
			return -1;
		}
		return 1;
	}
	
	
	public Hobby getHobby(int id)
	{
		return irp.getHobby().get(id);
		
	}


}
