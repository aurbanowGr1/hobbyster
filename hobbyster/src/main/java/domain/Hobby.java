package domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.validation.constraints.Size;

@Entity
@NamedQueries({
@NamedQuery(name="Hobby.getAll", query="Select h from Hobby h"),
@NamedQuery(name="Hobby.findByHobby", query="Select h from Hobby h where h.interestedIn = ?1"),
})
public class Hobby extends EntityBase {
	
	@Size(min = 4, max = 15)
	private String interestedIn;

	
	@ManyToMany(mappedBy="Hobby")  
	private List<User> users;
	
	@OneToMany(mappedBy="Hobby")
	private List<Forum> forum;
	
	public Hobby()
	{
		users= new ArrayList<User>();
		forum = new ArrayList<Forum>();
		
	}




	public String getInterestedIn() {
		return interestedIn;
	}

	public void setInterestedIn(String interestedIn) {
		this.interestedIn = interestedIn;
	}

	public List<User> getUsers() {
		return users;
	}
	
	public void addUser(User user) {
		this.users.add(user);
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	public List<Forum> getForum() {
		return forum;
	}

	public void setForum
	(List<Forum> forum) {
		this.forum = forum;
	}


	public void addForum(Forum forum) {
		this.forum.add(forum);
	}	
	
	
}
