package domain;

import java.util.*;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.validation.constraints.Size;


@Entity
@NamedQueries({
@NamedQuery(name="Forum.getAll", query="Select f from Forum f"),
@NamedQuery(name="Forum.findByForumName", query="Select f from Forum f where f.forumName = ?1"),
})

public class Forum extends EntityBase{

	@Size(min = 5, max = 15)
	private String forumName;
	
	@ManyToOne
	private Hobby hobby;
	
	@ManyToMany(mappedBy="Forum") 
	private List<User> users;
		
	@OneToMany(mappedBy="Forum")
	private List<Messenger> message;
	
	public Forum()
	{
		users=new ArrayList<User>();
		message=new ArrayList<Messenger>();
	}



	public String getForumName() {
		return forumName;
	}

	public void setForumName(String groupName) {
		this.forumName = groupName;
	}

	public Hobby getHobby() {
		return hobby;
	}

	public void setHobby(Hobby hobby) {
		this.hobby = hobby;
	}

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}
	
	public void addUser(User user) {
		this.users.add(user);
	}

	public List<Messenger> getMessage() {
		return message;
	}

	public void setMessage(List<Messenger> message) {
		this.message = message;
	}

	public void addMessage(Messenger message) {
		this.message.add(message);
	}
	
	
}

	