package domain;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.validation.constraints.Size;

@Entity
@NamedQueries({
@NamedQuery(name="Messenger.getAll", query="Select m from Messenger m"),
@NamedQuery(name="Messenger.findByMessage", query="Select  m from Messenger m where m.message = ?1"),
})

public class Messenger extends EntityBase {

	@Size(min = 10, max = 200)
	private String message;
	
	@ManyToOne
	private Forum groups;
	@ManyToOne
	private User users;



	public String getMessage() {
		return message;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}

	
	public Forum getGroupFrom() {
		return groups;
	}
	
	public void setGroupsFrom(Forum groups) {
		this.groups = groups;
	}
	public Forum getGroupTo() {
		return groups;
	}
	
	public void setGroupsTo(Forum groups) {
		this.groups = groups;
	}
	
	public User getUserFrom() {
		return users;
	}
	
	public void setUsersFrom(User users) {
		this.users = users;
	}
	public User getUserTo() {
		return users;
	}
	
	public void setUsersTo(User users) {
		this.users = users;
	}
	

	
	

	

}
