package domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.validation.constraints.Size;

@Entity
@NamedQueries({
@NamedQuery(name="User.getAll", query="Select u from User u"),
@NamedQuery(name="User.findByNick", query="Select u from User u where u.nick = ?1"),
@NamedQuery(name="User.findByName", query="Select u from User u where u.name = ?1"),
@NamedQuery(name="User.findByAge", query="Select u from User u where u.age = ?1"),
@NamedQuery(name="User.findByCiry", query="Select u from User u where u.city = ?1"),
})
public class User extends EntityBase{

	@Size(min = 4, max = 10)
	private String name;
	@Size(min = 6, max = 50)
	private String mail;
	@Size(min = 4, max = 10)
	private String nick;
	@Size(min = 3, max = 15)
	private String password;
	private String age;
	@Size(min = 5, max = 15)
	private String city;
	
	@ManyToMany
	@JoinTable(name = "user_forum",
	joinColumns=
			@JoinColumn(name="user_id", referencedColumnName="id"),
		      inverseJoinColumns=@JoinColumn(name="forum_id", referencedColumnName="id")) 
	private List<Forum> forum;
	
	@ManyToMany
	@JoinTable(name="user_hobby",
			joinColumns=@JoinColumn(name="user_id", referencedColumnName="id"),
		      inverseJoinColumns=@JoinColumn(name="hobby_id", referencedColumnName="id")) 
	private List<Hobby> hobbys;
	
	 @OneToMany(mappedBy="User")
	private List<Messenger> message;
	 
	
	public User()
	{ 
		forum=new ArrayList<Forum>();
		hobbys=new ArrayList<Hobby>();
		message=new ArrayList<Messenger>();
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getMail() {
		return mail;
	}


	public void setMail(String mail) {
		this.mail = mail;
	}


	public String getNick() {
		return nick;
	}


	public void setNick(String nick) {
		this.nick = nick;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}


	public String getAge() {
		return age;
	}


	public void setAge(String age) {
		this.age = age;
	}


	public String getCity() {
		return city;
	}


	public void setCity(String city) {
		this.city = city;
	}


	public List<Forum> getForum() {
		return forum;
	}


	public void setForum(List<Forum> forum) {
		this.forum = forum;
	}


	public List<Hobby> getHobbys() {
		return hobbys;
	}


	public void setHobbys(List<Hobby> hobbys) {
		this.hobbys = hobbys;
	}

	public void addForum(Forum forum) {
		this.forum.add(forum);
	}
	
	public void addHobby(Hobby hobby) {
		this.hobbys.add(hobby);
	}


	public List<Messenger> getMessage() {
		return message;
	}


	public void setMessage(List<Messenger> message) {
		this.message = message;
	}
	
	public void addMessage(Messenger message) {
		this.message.add(message);
	}

	

}


	